# XLabbers dot xyz website

This site was originally set up as a resource following the large set of layoffs in [February 2023](https://about.gitlab.com/blog/2023/02/09/gitlab-news/) to connect impacted team-members with former team members who had moved on to places with open positions.

Created by [Brendan O'Leary](https://www.linkedin.com/in/olearycrew/) it was basically a link to two Google Sheets: Folks hiring and folks seeking employment. From there we're hoping to continue to build a community of like-minded GitLabbers for life (ex-team members or XLabbers) who want to connect, have a community, and maybe even work together again some day.

As with all things GitLab it's very open, feel free to contribute a merge request or open an issue. Tag `@brendan` with any questions.

## Production

Production runs in Cloudflare pages at [https://xlabbers.xyz](https://xlabbers.xyz)

## Development

XLabbers.xyz is built with [Astro](https://astro.build). To run it locally:

- Clone the repository
- Run `npm install`
- Run `npm run dev`
- Visit http://localhost:3000

If you'd like to contribute to the project, open a Merge Request. Also you can request access to the project to have direct repo access (and not have to fork like some sort of GitHubber)